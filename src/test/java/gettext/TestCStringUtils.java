package gettext;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import me.vinceh121.gettext.CStringUtils;

public class TestCStringUtils {

	@Test
	public void unquote() {
		assertEquals("owo", CStringUtils.unquote("\"owo\""));
	}
}
