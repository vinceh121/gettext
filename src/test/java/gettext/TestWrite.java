package gettext;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.junit.jupiter.api.Test;

import me.vinceh121.gettext.POEntry;
import me.vinceh121.gettext.POReference;
import me.vinceh121.gettext.POWriter;

public class TestWrite {
	@Test
	public void testRead() throws IOException {
		String expected
				= new String(new BufferedInputStream(TestWrite.class.getClassLoader().getResourceAsStream("test.po"))
						.readAllBytes());

		ByteArrayOutputStream buf = new ByteArrayOutputStream();

		POWriter writer = new POWriter(buf);

		POEntry firstExpected = new POEntry();
		firstExpected.getReferences().add(POReference.parse("lib/error.c:116"));
		firstExpected.setMsgId("Unknown system error");
		firstExpected.setMsgStr("Error desconegut del sistema");

		POEntry secondExpected = new POEntry();
		secondExpected.setCommentTranslator("this is a translator comment on\na Japanese string");
		secondExpected.setMsgId("Failed to fetch user data");
		secondExpected.setMsgStr("ユーザデータの取得失敗");

		writer.writeEntry(firstExpected);
		writer.writeEntry(secondExpected);
		writer.flush();
		writer.close();

		assertEquals(expected, new String(buf.toByteArray()));
	}
}
