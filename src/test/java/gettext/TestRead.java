package gettext;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.io.InputStreamReader;

import org.junit.jupiter.api.Test;

import me.vinceh121.gettext.POEntry;
import me.vinceh121.gettext.POReader;
import me.vinceh121.gettext.POReference;

public class TestRead {
	@Test
	public void testRead() throws IOException {
		POEntry firstExpected = new POEntry();
		firstExpected.getReferences().add(POReference.parse("lib/error.c:116"));
		firstExpected.setMsgId("Unknown system error");
		firstExpected.setMsgStr("Error desconegut del sistema");

		POEntry secondExpected = new POEntry();
		secondExpected.setCommentTranslator("this is a translator comment on\na Japanese string");
		secondExpected.setMsgId("Failed to fetch user data");
		secondExpected.setMsgStr("ユーザデータの取得失敗");

		try (POReader reader
				= new POReader(new InputStreamReader(TestRead.class.getClassLoader().getResourceAsStream("test.po")))) {
			POEntry firstActual = reader.nextEntry();
			POEntry secondActual = reader.nextEntry();
			assertEquals(firstExpected, firstActual);
			assertEquals(secondExpected, secondActual);
		}
	}
}
