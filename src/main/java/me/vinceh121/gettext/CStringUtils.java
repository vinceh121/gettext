package me.vinceh121.gettext;

import org.apache.commons.text.StringEscapeUtils;

public final class CStringUtils {
	public static String unquote(String src) {
		if (src.startsWith("\"") && src.endsWith("\"")) {
			return src.substring(1, src.length() - 1);
		}
		return src;
	}

	public static String stringBlock(String src) {
		StringBuilder sb = new StringBuilder();
		String[] lines = src.split("\n");
		for (String l : lines) {
			sb.append("\"");
			sb.append(StringEscapeUtils.escapeJava(l));
			sb.append("\"");
			sb.append("\n");
		}
		return sb.toString();
	}
}
