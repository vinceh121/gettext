package me.vinceh121.gettext;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.nio.charset.Charset;

public class POWriter extends PrintWriter {

	public POWriter(File file, Charset charset) throws IOException {
		super(file, charset);
	}

	public POWriter(File file, String csn) throws FileNotFoundException, UnsupportedEncodingException {
		super(file, csn);
	}

	public POWriter(File file) throws FileNotFoundException {
		super(file);
	}

	public POWriter(OutputStream out, boolean autoFlush, Charset charset) {
		super(out, autoFlush, charset);
	}

	public POWriter(OutputStream out, boolean autoFlush) {
		super(out, autoFlush);
	}

	public POWriter(OutputStream out) {
		super(out);
	}

	public POWriter(String fileName, Charset charset) throws IOException {
		super(fileName, charset);
	}

	public POWriter(String fileName, String csn) throws FileNotFoundException, UnsupportedEncodingException {
		super(fileName, csn);
	}

	public POWriter(String fileName) throws FileNotFoundException {
		super(fileName);
	}

	public POWriter(Writer out, boolean autoFlush) {
		super(out, autoFlush);
	}

	public POWriter(Writer out) {
		super(out);
	}

	public void writeEntry(POEntry entry) {
		if (entry.getCommentTranslator() != null)
			this.writeComment("# ", entry.getCommentTranslator());
		if (entry.getCommentExtracted() != null)
			this.writeComment("#. ", entry.getCommentExtracted());
		for (POReference ref : entry.getReferences()) {
			this.print("#: ");
			this.println(ref.toString());
		}

		if (entry.getFlags().size() > 0) {
			this.print("#, ");
			for (int i = 0; i < entry.getFlags().size(); i++) {
				this.print(entry.getFlags().get(i).toString());
				if (i != entry.getFlags().size() - 1) {
					this.print(", ");
				}
			}
			println();
		}

		// TODO previous entry

		if (entry.getMsgId() != null) {
			print("msgid ");
			print(CStringUtils.stringBlock(entry.getMsgId()));
		}

		if (entry.getMsgIdPlural() != null) {
			print("msgid_plural ");
			print(CStringUtils.stringBlock(entry.getMsgIdPlural()));
		}

		if (entry.getMsgStr() != null) {
			print("msgstr ");
			print(CStringUtils.stringBlock(entry.getMsgStr()));
		} else if (entry.getMsgStrList().size() != 0) {
			for (int i = 0; i < entry.getMsgStrList().size(); i++) {
				print("msgtr[");
				print(i);
				print("] ");
				print(CStringUtils.stringBlock(entry.getMsgStrList().get(i)));
			}
		} else {
			throw new IllegalArgumentException("Neither msgstr nor msgstr list are set");
		}

		println();
	}

	private void writeComment(String prefix, String text) {
		String[] commentParts = text.split("\n");
		for (String s : commentParts) {
			print(prefix);
			println(s);
		}
	}
}
