package me.vinceh121.gettext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;

import org.apache.commons.text.StringEscapeUtils;

import me.vinceh121.gettext.POReference.ReferenceParser;

public class POReader extends BufferedReader {
	private String previousPropertyType;
	private ReferenceParser referenceParser = new ReferenceParser();;

	public POReader(Reader in, int sz) {
		super(in, sz);
	}

	public POReader(Reader in) {
		super(in);
	}

	public POEntry nextEntry() throws IOException {
		final POEntry entry = new POEntry();
		String line = readLine();

		if (line == null) {
			return null;
		}

		do {
			if (line == null)
				return entry;
			this.handleLine(entry, line);
		} while (!"".equals(line = readLine()));

		return entry;
	}

	private void handleLine(POEntry entry, String line) {
		if (line.startsWith("#")) {
			this.handleComment(entry, line);
		} else {
			this.handleProperty(entry, line);
		}
	}

	private void handleProperty(POEntry entry, String line) {
		if (line.startsWith("\"")) {
			if (this.previousPropertyType == null) {
				throw new IllegalStateException("Encountered blockline without property start");
			} else {
				this.handleBlockProperty(entry, line);
			}
		} else if (line.startsWith("msgid_plural")) {
			this.previousPropertyType = "msgid_plural";
			entry.setMsgIdPlural(CStringUtils.unquote(StringEscapeUtils.UNESCAPE_JAVA.translate(line.substring(13))));
		} else if (line.startsWith("msgid")) {
			this.previousPropertyType = "msgid";
			entry.setMsgId(CStringUtils.unquote(StringEscapeUtils.UNESCAPE_JAVA.translate(line.substring(6))));
		} else if (line.startsWith("msgstr")) {
			this.previousPropertyType = "msgstr";
			entry.setMsgStr(CStringUtils.unquote(StringEscapeUtils.UNESCAPE_JAVA.translate(line.substring(7))));
		}
	}

	private void handleBlockProperty(POEntry entry, String line) {
		if (this.previousPropertyType.equals("msgid_plural")) {
			entry.setMsgIdPlural(entry.getMsgIdPlural()
					+ "\n"
					+ CStringUtils.unquote(StringEscapeUtils.UNESCAPE_JAVA.translate(line)));
		} else if (this.previousPropertyType.equals("msgid")) {
			entry.setMsgId(
					entry.getMsgId() + "\n" + CStringUtils.unquote(StringEscapeUtils.UNESCAPE_JAVA.translate(line)));
		} else if (this.previousPropertyType.equals("msgstr")) {
			entry.setMsgStr(
					entry.getMsgStr() + "\n" + CStringUtils.unquote(StringEscapeUtils.UNESCAPE_JAVA.translate(line)));
		}
	}

	private void handleComment(POEntry entry, String line) {
		if (line.startsWith("#.")) {
			String comment = line.substring(2).trim();
			if (entry.getCommentExtracted() == null)
				entry.setCommentExtracted(comment);
			else
				entry.setCommentExtracted(entry.getCommentExtracted() + "\n" + comment);
		} else if (line.startsWith("#:")) {
			String ref = line.substring(2).trim();
			entry.getReferences().add(this.referenceParser.parse(ref));
		} else if (line.startsWith("#,")) {
			String[] flagCsv = line.substring(2).split(", ");
			for (String rawFlag : flagCsv) {
				POFlag flag = POFlag.fromPOFormat(rawFlag.trim());
				entry.getFlags().add(flag);
			}
		} else if (line.startsWith("#|")) {
			String deepLine = line.substring(2).trim();
			if (entry.getPreviousEntry() == null) {
				entry.setPreviousEntry(new POEntry());
			}
			this.handleLine(entry.getPreviousEntry(), deepLine);
		} else if (line.startsWith("#")) {
			String comment = line.substring(1).trim();
			if (entry.getCommentTranslator() == null) {
				entry.setCommentTranslator(comment);
			} else {
				entry.setCommentTranslator(entry.getCommentTranslator() + "\n" + comment);
			}
		}
	}

	public ReferenceParser getReferenceParser() {
		return referenceParser;
	}

	public void setReferenceParser(ReferenceParser referenceParser) {
		this.referenceParser = referenceParser;
	}
}
