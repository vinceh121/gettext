package me.vinceh121.gettext;

import java.util.ArrayList;
import java.util.List;

public class POEntry {
	/**
	 * The translator comments is the string that follows a single {@code #}
	 */
	private String commentTranslator;
	/**
	 * The extracted comments is the string that follows a {@code #.}
	 *
	 * These are extracted from the code by xgettext
	 */
	private String commentExtracted;
	/**
	 * The reference is a string pointing to where the string can be found in the
	 * code base.
	 *
	 * Typically following the format {@code relativePath:lineNumber} e.g.
	 * {@code lib/error.c:116}
	 */
	private List<POReference> references = new ArrayList<>();
	/**
	 * These are flags used by msgfmt to give diagnostic messages.
	 *
	 * It is a comma-separated list that follows a {@code #,}
	 */
	private List<POFlag> flags = new ArrayList<>();
	/**
	 * This is the previous entry reconstituted from comments that follow {@code #|}
	 */
	private POEntry previousEntry;

	private String msgId;
	private String msgIdPlural;
	/**
	 * {@code null} when a list of strings is specified.
	 *
	 * @see #msgStrList
	 */
	private String msgStr;
	/**
	 * Empty when a single string is specified.
	 *
	 * @see #msgStr
	 */
	private List<String> msgStrList = new ArrayList<>();

	public String getCommentTranslator() {
		return commentTranslator;
	}

	public void setCommentTranslator(String commentTranslator) {
		this.commentTranslator = commentTranslator;
	}

	public String getCommentExtracted() {
		return commentExtracted;
	}

	public void setCommentExtracted(String commentExtracted) {
		this.commentExtracted = commentExtracted;
	}

	public List<POReference> getReferences() {
		return references;
	}

	public void setReferences(List<POReference> references) {
		this.references = references;
	}

	public List<POFlag> getFlags() {
		return flags;
	}

	public void setFlags(List<POFlag> flags) {
		this.flags = flags;
	}

	public POEntry getPreviousEntry() {
		return previousEntry;
	}

	public void setPreviousEntry(POEntry previousEntry) {
		this.previousEntry = previousEntry;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public String getMsgStr() {
		return msgStr;
	}

	public void setMsgStr(String msgStr) {
		this.msgStr = msgStr;
	}

	public String getMsgIdPlural() {
		return msgIdPlural;
	}

	public void setMsgIdPlural(String msgIdPlural) {
		this.msgIdPlural = msgIdPlural;
	}

	public List<String> getMsgStrList() {
		return msgStrList;
	}

	public void setMsgStrList(List<String> msgStrList) {
		this.msgStrList = msgStrList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((commentExtracted == null) ? 0 : commentExtracted.hashCode());
		result = prime * result + ((commentTranslator == null) ? 0 : commentTranslator.hashCode());
		result = prime * result + ((flags == null) ? 0 : flags.hashCode());
		result = prime * result + ((msgId == null) ? 0 : msgId.hashCode());
		result = prime * result + ((msgStr == null) ? 0 : msgStr.hashCode());
		result = prime * result + ((msgStrList == null) ? 0 : msgStrList.hashCode());
		result = prime * result + ((previousEntry == null) ? 0 : previousEntry.hashCode());
		result = prime * result + ((references == null) ? 0 : references.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		POEntry other = (POEntry) obj;
		if (commentExtracted == null) {
			if (other.commentExtracted != null)
				return false;
		} else if (!commentExtracted.equals(other.commentExtracted))
			return false;
		if (commentTranslator == null) {
			if (other.commentTranslator != null)
				return false;
		} else if (!commentTranslator.equals(other.commentTranslator))
			return false;
		if (flags == null) {
			if (other.flags != null)
				return false;
		} else if (!flags.equals(other.flags))
			return false;
		if (msgId == null) {
			if (other.msgId != null)
				return false;
		} else if (!msgId.equals(other.msgId))
			return false;
		if (msgStr == null) {
			if (other.msgStr != null)
				return false;
		} else if (!msgStr.equals(other.msgStr))
			return false;
		if (msgStrList == null) {
			if (other.msgStrList != null)
				return false;
		} else if (!msgStrList.equals(other.msgStrList))
			return false;
		if (previousEntry == null) {
			if (other.previousEntry != null)
				return false;
		} else if (!previousEntry.equals(other.previousEntry))
			return false;
		if (references == null) {
			if (other.references != null)
				return false;
		} else if (!references.equals(other.references))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "POEntry [commentTranslator="
				+ commentTranslator
				+ ", commentExtracted="
				+ commentExtracted
				+ ", references="
				+ references
				+ ", flags="
				+ flags
				+ ", previousEntry="
				+ previousEntry
				+ ", msgId="
				+ msgId
				+ ", msgStr="
				+ msgStr
				+ ", msgStrList="
				+ msgStrList
				+ "]";
	}
}
