package me.vinceh121.gettext;

import java.util.regex.Pattern;

public class POReference {
	private String filePath;
	private int line;

	public POReference() {
	}

	public POReference(String filePath, int line) {
		this.filePath = filePath;
		this.line = line;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public int getLine() {
		return line;
	}

	public void setLine(int line) {
		this.line = line;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((filePath == null) ? 0 : filePath.hashCode());
		result = prime * result + line;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		POReference other = (POReference) obj;
		if (filePath == null) {
			if (other.filePath != null)
				return false;
		} else if (!filePath.equals(other.filePath))
			return false;
		if (line != other.line)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return this.filePath + ":" + this.line;
	}

	public static POReference parse(String ref) {
		return new ReferenceParser().parse(ref);
	}

	public static class ReferenceParser {
		public POReference parse(String source) {
			POReference ref = new POReference();
			String[] parts = source.split(Pattern.quote(":"));
			if (parts.length != 2) {
				throw new IllegalArgumentException("Invalid count of ':'");
			}
			ref.setFilePath(parts[0]);
			ref.setLine(Integer.parseInt(parts[1]));
			return ref;
		}
	}
}
